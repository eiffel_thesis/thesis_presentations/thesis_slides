\newif \ifshare
% \sharetrue % Comment this if you want animation
\ifshare % "Share mode" without animation.
\documentclass[table, trans, aspectratio = 169]{beamer}
\else % "Presentation mode" with animation.
\documentclass[table, aspectratio = 169]{beamer}
\fi
\usepackage{default}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{rotating}
\usepackage{bytefield}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{pifont}
\usepackage{diagbox}


\let\pgfmathMod=\pgfmathmod\relax

\sisetup{binary-units=true}

\graphicspath{{figs/PDF/}}

\usetheme{Boadilla}

\definecolor{struct}{HTML}{03a9f4}
\definecolor{alert}{HTML}{f44336}
\definecolor{example}{HTML}{aeea00}
\definecolor{good}{HTML}{8bc34a}
\definecolor{notgoodnotbad}{HTML}{ff9800}

\setbeamercolor{structure}{fg = struct}
\setbeamercolor{normal text}{fg = white, bg = black}
\setbeamercolor{example text}{fg = example}
\setbeamercolor{alerted text}{fg = alert}
\setbeamercolor{footline}{fg = white}

\title[Soutenance de thèse]{\textbf{MemOpLight} :\\ Vers une consolidation mémoire pour les conteneurs grâce à un retour applicatif}
\subtitle{Thèse de Doctorat de Sorbonne Université}
\author{Francis Laniel}
\date{9 novembre 2020}

% This need the pifont package.
% Information was taken from: https://tex.stackexchange.com/a/42620.
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\newcommand{\step}[3]{$\varphi_{#1}(#2, #3)$}
\newcommand{\stepeight}[1]{$\varphi_#1$}

% Custom title page.
\defbeamertemplate*{title page}{customized}[1][]{
	\centering
	\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle\par
	\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par
	\bigskip
	\usebeamerfont{author}\usebeamercolor[fg]{normal text}\textbf{\insertauthor}\par
	\bigskip
	\usebeamerfont{date}\usebeamercolor[fg]{normal text}\textbf{\insertdate}\par
	\bigskip

	\begin{centering}
		\scriptsize
		Membres du jury :

		\begin{tabular}{cc}
			Sonia BEN MOKHTAR & Rapportrice\\
			Gaël THOMAS & Rapporteur\\
			Thomas LEDOUX & Examinateur\\
			Pierre SENS & Examinateur\\
			Marc SHAPIRO & Directeur de thèse\\
			Jonathan LEJEUNE & Encadrant\\
			Julien SOPENA & Encadrant\\
			Franck WAJSBURT & Encadrant\\
		\end{tabular}
	\end{centering}
	\bigskip

	% Makebox takes two options:
	% 1. First is the width, setting 0 means that the box has the size of the
	% content.
	% 2. Second is the text alignement.
	% We use it to display 3 elements aligned correctly.
	% This tip was taken from:
	% https://tex.stackexchange.com/a/223348
	\makebox[0pt][l]{\includegraphics[scale = .5]{sorbonne.pdf}}%
	\hfill%
	\makebox[0pt][c]{\includegraphics[scale = .05]{lip6.pdf}}%
	\hfill%
	\makebox[0pt][r]{\includegraphics[scale = .5]{inria.pdf}}%
}

\begin{document}
	% Put these parameters here to avoid compilation error:
	% "! LaTeX Error: Missing \begin{document}."
	% Remove the navigation bar, this is useless...
	\setbeamertemplate{navigation symbols}{}
	% Use square instead of bubbles, see:
	% https://tex.stackexchange.com/a/69721
	\setbeamertemplate{section in toc}[square]
	% Modify the shaded value to 60% instead of 20%, see:
	% https://tex.stackexchange.com/a/66703
	\setbeamertemplate{sections/subsections in toc shaded}[default][50]
	% Use circle instead of bubbles for itemize, see:
	% \setbeamertemplate{itemize items}[circle]
	\setbeamertemplate{itemize items}[circle]

	\maketitle

	\section{Le \textit{cloud}}
	\begin{frame}
		\frametitle{Le \textit{cloud}}
		\framesubtitle{Introduction}

		Le \textit{cloud} consiste en \cite{amazon_quest_ce_nodate} :
		\begin{quote}
			[...] la mise à disposition de ressources informatiques à la demande via Internet, avec une tarification en fonction de votre utilisation.
		\end{quote}

		\bigskip

		Pour le client, il permet \cite{amazon_quest_ce_nodate} :
		\begin{itemize}
			\item L'agilité
			\item L'élasticité
			\item La réduction des coûts
			\item Le déploiement mondiale en quelques instants
		\end{itemize}

		\bigskip

		Le fournisseur assure une qualité de service au client \cite{microsoft_sla_nodate, ovh_conditions_nodate, amazon_sla_nodate}.
	\end{frame}

	\begin{frame}
		\frametitle{Le \textit{cloud}}
		\framesubtitle{Des objectifs divergents}

		\textcolor{struct}{Pour le client :} Avoir les ressources pour lesquelles il a payées.

		\textcolor{struct}{Pour le fournisseur :} Maximiser le nombre de client pour un nombre de ressource donné tout en assurant une qualité de service.

		\bigskip

		\onslide<2>{
			\textcolor{struct}{SLA :} \textit{Service Level Agreement}/Entente de Niveau de Service.

			\textcolor{struct}{SLO :} \textit{Service Level Objective}/Objectifs de Niveau de Service.
		}
	\end{frame}

	\begin{frame}
		\frametitle{Fondements technologiques : la virtualisation \cite{popek_formal_1974}\cite[Section 1.4]{bugnion_hardware_2017}\cite{docker_what_nodate}}

		\begin{columns}
			\begin{column}{.33\textwidth}<1->
				\centering

				\includegraphics[scale = .33]{hypervisor_type1.pdf}

				Type 1 (\texttt{xen}, \texttt{ESXi}, etc.)
			\end{column}
			\begin{column}{.33\textwidth}<2->
				\centering

				\includegraphics[scale = .33]{hypervisor_type2.pdf}

				Type 2 (\texttt{vmware}, \texttt{virtualbox}, etc.)
			\end{column}
			\begin{column}{.33\textwidth}<3>
				\centering

				\includegraphics[scale = .33]{container.pdf}

				Container (\texttt{docker}, \texttt{lxc}, \texttt{podman}, etc.)
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Les conteneurs et la mémoire}

		\begin{columns}
			\begin{column}{.5\textwidth}
				Les conteneurs s'appuient sur plusieurs fonctionnalités du noyau :
				\begin{description}
					\item[Les \texttt{namespaces} :] Isolation de sécurité \cite{rami_rosen_namespace_2016, kerrisk_lce_2012, kerrisk_namespaces_2013}.
					\item[Les \texttt{cgroups} :] Isolation de ressources \cite{rami_rosen_namespace_2016, hiroyu_cgroup_2008}.
				\end{description}

				\bigskip

				Le \texttt{cgroup} mémoire offre deux limites :
				\begin{itemize}
					\item \texttt{max limit :} Impossible à outrepasser.
					\item \texttt{soft limit :} Impossible à outrepasser sous pression mémoire.
				\end{itemize}
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[scale = .42]{container.pdf}

				Container (\texttt{docker}, \texttt{lxc}, \texttt{podman}, etc.)
			\end{column}
		\end{columns}
	\end{frame}

	\section{La mémoire}
	\begin{frame}
		\frametitle{Plan}

		\tableofcontents[currentsection]
	\end{frame}

	\begin{frame}
		\frametitle{La mémoire}
		\framesubtitle{Mémoire anonyme \textit{vs.} mémoire fichier}

		\includegraphics<1>[scale = .4]{memory_cache-fig1.pdf}%
		\includegraphics<2>[scale = .4]{memory_cache-fig2.pdf}%
		\includegraphics<3>[scale = .4]{memory_cache-fig3.pdf}%
		\includegraphics<4->[scale = .4]{memory_cache-fig4.pdf}%

		\begin{scriptsize}
			\begin{columns}
				\begin{column}{.5\textwidth}<2->
					Mémoire anonyme :
					\begin{itemize}
						\item Stocke le code, la pile et le tas (\texttt{malloc}).
						\item S'il n'est pas possible d'allouer de la mémoire anonyme, le programme s'arrête.
					\end{itemize}
				\end{column}
				\begin{column}{.5\textwidth}<4>
					Mémoire fichier :
					\begin{itemize}
						\item Sert de cache aux données lues depuis le disque.
						\item Utilise toute la mémoire disponible non allouée.
					\end{itemize}
				\end{column}
			\end{columns}
		\end{scriptsize}
	\end{frame}

	\begin{frame}
		\frametitle{La mémoire}
		\framesubtitle{Particularité du \texttt{cgroup} mémoire \cite{corbet_integrating_2011, rik_van_riel_patch_2008}}

		\includegraphics<1>[scale = .4]{memory_cache-fig4.pdf}%
		\includegraphics<2>[scale = .4]{memory_cache-fig5.pdf}%
		\includegraphics<3>[scale = .4]{memory_cache-fig6.pdf}%
		\includegraphics<4>[scale = .4]{memory_cache-fig7.pdf}%
		\includegraphics<5>[scale = .4]{memory_cache-fig8.pdf}%

		\only<5>{
			Problème :
			\begin{itemize}
				\item En cas de pression mémoire globale, tous les \texttt{cgroups} sont réclamés.
				\item Il n'est donc pas possible de cibler un \texttt{cgroup} en particulier.
			\end{itemize}
		}
	\end{frame}

	\section{Le problème des conteneurs : l'isolation sans la consolidation}
	\begin{frame}
		\frametitle{Plan}

		\tableofcontents[currentsection]
	\end{frame}

	\begin{frame}
		\frametitle{L'isolation sans la consolidation}
		\framesubtitle{Définition}

		\begin{columns}
			\begin{column}{.33\textwidth}
				\centering

				Sans limite

				\bigskip

				\includegraphics<1>[scale = .25]{no_limit-fig1.pdf}%
				\includegraphics<2>[scale = .25]{no_limit-fig2.pdf}%
				\includegraphics<3->[scale = .25]{no_limit-fig3.pdf}%
			\end{column}
			\begin{column}{.33\textwidth}<4->
				\centering

				\texttt{Max} limite

				\bigskip

				\includegraphics<4>[scale = .25]{max_limit-fig1.pdf}%
				\includegraphics<5->[scale = .25]{max_limit-fig2.pdf}%
			\end{column}
			\begin{column}{.33\textwidth}<6->
				\centering

				\texttt{Soft} limite

				\bigskip

				\includegraphics<6>[scale = .25]{soft_limit-fig1.pdf}%
				\includegraphics<7>[scale = .25]{soft_limit-fig2.pdf}%
				\includegraphics<8->[scale = .25]{soft_limit-fig3.pdf}%
			\end{column}
		\end{columns}

		\bigskip

		\only<9>{
			\centering

			\textcolor{struct}{Quelle est l'efficacité de ces mécanismes par rapport à l'isolation et la consolidation ?}
		}
	\end{frame}

	\begin{frame}
		\frametitle{L'isolation sans la consolidation}
		\framesubtitle{Environement de l'expérience \cite{alexey_kopytov_sysbench_nodate, thesis_sysbench, noauthor_mysql_nodate, grid5000, intel_intel_2017, qemu_qemu_2019, qemu_qemu_2019, noauthor_docker_py_nodate}}

		\centering

		\includegraphics<1>[scale = .33]{experiment_setup-fig1.pdf}%
		\includegraphics<2>[scale = .33]{experiment_setup-fig2.pdf}%
		\includegraphics<3>[scale = .33]{experiment_setup-fig3.pdf}%
		\includegraphics<4>[scale = .33]{experiment_setup-fig4.pdf}%
		\includegraphics<5>[scale = .33]{experiment_setup-fig5.pdf}%
	\end{frame}

	\begin{frame}
		\frametitle{L'isolation sans la consolidation}
		\framesubtitle{Présentation de l'expérience}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[scale = .42]{dummy_transactions-fig1.pdf}%
				\includegraphics<2>[scale = .42]{dummy_transactions-fig2.pdf}%
				\includegraphics<3>[scale = .42]{dummy_transactions-fig3.pdf}%
				\includegraphics<4>[scale = .42]{dummy_transactions-fig4.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[scale = .42]{dummy_memory-fig1.pdf}%
				\includegraphics<2>[scale = .42]{dummy_memory-fig2.pdf}%
				\includegraphics<3->[scale = .42]{dummy_memory-fig3.pdf}%

				Mémoire
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}[t]
		\frametitle{L'isolation sans la consolidation}
		\framesubtitle{Résultats de l'expérience}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig6.pdf}%
				\includegraphics<7->[width = \textwidth, height = .35\textheight]{pair_default_transactions-fig7.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_default_memory-fig6.pdf}%
				\includegraphics<7->[width = \textwidth, height = .35\textheight]{pair_default_memory-fig7.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			Sans limite
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_max_transactions-fig7.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig7.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_max_memory-fig7.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig7.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			\texttt{\onslide<-7>{Max}\only<8>{Soft}} limite
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{L'isolation sans la consolidation}
		\framesubtitle{Conclusion}

		\begin{table}
			\centering

			\begin{tabular}{|l|c|c|}
				\hline
				Mécanisme utilisé & Consolidation & Isolation\\
				\hline
				limite non renseignée & \textcolor{notgoodnotbad}{faible} & \textcolor{alert}{non}\\
				\hline
				\texttt{max} limite & \textcolor{alert}{non} & \textcolor{good}{oui}\\
				\hline
				\texttt{soft} limite & \textcolor{notgoodnotbad}{non (sous pression mémoire)} & \textcolor{good}{oui}\\
				\hline
			\end{tabular}
		\end{table}

		\bigskip

		\onslide<2->{
			\centering

			La \texttt{soft} limit consolide la mémoire \textbf{pas} utilisée et non la mémoire \textbf{peu} utilisée.
		}

		\bigskip

		\only<3>{
			\centering

			\textcolor{struct}{Comment consolider la mémoire peu utilisée tout en assurant l'isolation ?}
		}
	\end{frame}

	\section{MemOpLight}
	\begin{frame}
		\frametitle{Plan}

		\tableofcontents[currentsection]
	\end{frame}

	\begin{frame}
		\frametitle{MemOpLight}
		\framesubtitle{La boucle MAPE-K \cite{horn_autonomic_2001}\cite[Figure 2]{kephart_vision_2003}}

		La boucle MAPE-K permet, entre autres, le dimensionnement d'un système.

		\begin{columns}
			\begin{column}{.5\textwidth}<2->
				Les étapes de la boucle :
				\begin{itemize}
					\item \textit{Monitor}
					\item<3-> \textit{Analysis}
					\item<4-> \textit{Plan}
					\item<5-> \textit{Execute}
				\end{itemize}
				\only<6>{S'appuient sur une connaissance (\textit{Knowledge}).}
			\end{column}
			\begin{column}{.5\textwidth}<2->
				\centering

				\includegraphics[scale = .42]{mapek_loop_original.pdf}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{MemOpLight}
		\framesubtitle{Une boucle MAPE ?}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\includegraphics<1>[scale = .2]{loop_user-fig1.pdf}%
				\includegraphics<2->[scale = .2]{loop_user-fig2.pdf}%
			\end{column}
			\begin{column}{.5\textwidth}
				\includegraphics<3>[scale = .2]{loop_kernel-fig1.pdf}%
				\includegraphics<4>[scale = .2]{loop_kernel-fig2.pdf}%
				\includegraphics<5>[scale = .2]{loop_kernel-fig3.pdf}%
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{MemOpLight}
		\framesubtitle{Une double boucle MAPE !}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\includegraphics<1>[scale = .2]{memoplight-fig1.pdf}%
				\includegraphics<2>[scale = .2]{memoplight-fig2.pdf}%
				\includegraphics<3>[scale = .2]{memoplight-fig3.pdf}%
				\includegraphics<4>[scale = .2]{memoplight-fig4.pdf}%
				\includegraphics<5>[scale = .2]{memoplight-fig5.pdf}%
				\includegraphics<6->[scale = .2]{memoplight-fig6.pdf}%
			\end{column}
			\begin{column}{.5\textwidth}<7>
				\begin{tabular}{ccc}
					\hline
					Rouges & Jaunes & Réclamés\\
					\hline
					$0$ & $0$ & aucun\\
					$0$ & $\ge 1$ & verts\\
					$\ge 1$ & $0$ & verts\\
					$\ge 1$ & $\ge 1$ & verts puis jaunes\\
					\hline
				\end{tabular}
			\end{column}
		\end{columns}
	\end{frame}

	\section{Évaluation}
	\begin{frame}
		\frametitle{Plan}

		\tableofcontents[currentsection]
	\end{frame}

	\begin{frame}[t]
		\frametitle{Évaluation}
		\framesubtitle{Expérience à deux conteneurs}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_soft_transactions-fig7.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_soft_memory-fig7.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			\texttt{Soft} limite
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig7.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig2.pdf}%
				\includegraphics<3>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig3.pdf}%
				\includegraphics<4>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig4.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig5.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig6.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig7.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLight
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Expérience à deux conteneurs : conclusion}

		\begin{small}
			\begin{table}
				\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
					\hline
					Phases & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
					\hline
					Conteneur & A & B & A & B & A & B & A & B & A & B & B\\
					\hline
					Charge reçue & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
					\hline
					Sans limite & 1053 & \textcolor{struct}{1043} & 1374 & 196 & 1054 & \textcolor{struct}{1168} & 195 & 197 & 1195 & \textcolor{struct}{1145} & \textcolor{struct}{1751}\\
					\texttt{Max} limite & 1290 & 894 & 1411 & 196 & 1314 & 968 & 196 & 660 & 1358 & 978 & 962\\
					\texttt{Soft} limite & 1268 & 879 & 1423 & 197 & 1299 & 969 & 196 & 794 & 1360 & 970 & 974\\
					MemOpLight & \textcolor{struct}{1351} & 933 & \textcolor{struct}{1670} & 195 & \textcolor{struct}{1362} & 999 & 196 & 197 & \textcolor{struct}{1370} & 1036 & 1723\\
				\end{tabular}

				\caption{Débit}
			\end{table}
		\end{small}

		\only<2->{Conclusions :}
		\begin{itemize}
			\item<3-> MemOpLight est meilleur dans tous les cas...
			\item<4> Sauf pour le conteneur B quand il est exécuté sans limite mais ce n'est pas souhaitable !
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Expérience à huit conteneurs : présentation}

		\begin{columns}
			\begin{column}{.4\textwidth}
				Huit conteneurs de A à H :
				\begin{itemize}
					\item S'exécutent dans une VM de \SI{6}{\giga\byte}.
					\item Disposent, chacun, de deux cœurs.
					\item Utilisent, chacun, le benchmark \texttt{sysbench oltp}, \textit{modifié}, qui interragit avec \texttt{mysql 5.7.0} \cite{alexey_kopytov_sysbench_nodate, thesis_sysbench, noauthor_mysql_nodate}.
					\item Lisent donc, chacun, une base de données de \SI{2}{\giga\byte}.
					\item La charge de chaque conteneur varie toutes les \SI{120}{\second}.
				\end{itemize}
			\end{column}
			\begin{column}{.6\textwidth}<2>
				\begin{table}
					\begin{tabular}{ccc}
						\hline
						Étape & Phase(s) & Charge\\
						\hline
						1 & \stepeight{1} et \stepeight{2} & Forte\\
						2 & \stepeight{3} & Décroissante\\
						3 & \stepeight{4} & Faible\\
						4 & \stepeight{5} à \stepeight{9} & Croissante\\
						\hline
					\end{tabular}
				\end{table}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Évaluation}
		\framesubtitle{Expérience à huit conteneurs : résultats}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig2.pdf}%
				\includegraphics<3,4>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig3.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig4.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig5.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig6.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig7.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{soft_stacked_transactions-fig8.pdf}%

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<4,5>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig1.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig2.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig3.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig4.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{soft_limit_color-fig5.pdf}%

				\onslide<4->{Satisfaction}
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			\texttt{Soft} limite
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig2.pdf}%
				\includegraphics<3,4>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig3.pdf}%
				\includegraphics<5>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig4.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig5.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig6.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig7.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig8.pdf}%

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<4,5>[width = \textwidth, height = .35\textheight]{mechanism_color-fig1.pdf}%
				\includegraphics<6>[width = \textwidth, height = .35\textheight]{mechanism_color-fig2.pdf}%
				\includegraphics<7>[width = \textwidth, height = .35\textheight]{mechanism_color-fig3.pdf}%
				\includegraphics<8>[width = \textwidth, height = .35\textheight]{mechanism_color-fig4.pdf}%
				\includegraphics<9>[width = \textwidth, height = .35\textheight]{mechanism_color-fig5.pdf}%

				\onslide<4->{Satisfaction}
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLight
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Expérience à huit conteneurs : conclusion}

		\begin{table}
			\centering

			\begin{tabular}{l|cccccccc|c}
				\hline
				\diagbox{Mécanisme}{Conteneur} & A & B & C & D & E & F & G & H & Total\\
				\hline
				Aucune limite fixée & 15.2 & 14.5 & 17.3 & 11.9 & 18.7 & 15.9 & \textcolor{struct}{8.8} & \textcolor{struct}{10.3} & 112.6\\
				\texttt{Max} limite & 13.6 & 13.8 & 13.8 & 8.5 & 14.9 & 9.9 & 4.6 & 6.8 & 85.9\\
				\texttt{Soft} limite & 17.1 & \textcolor{struct}{17.4} & 17.6 & 12.6 & 17.5 & 13.4 & 7.3 & 9.5 & 112.4\\
				MemOpLight & \textcolor{struct}{17.5} & 17.3 & \textcolor{struct}{18.4} & \textcolor{struct}{13.7} & \textcolor{struct}{20.2} & \textcolor{struct}{16.8} & 8.6 & 10.1 & \textcolor{struct}{122.6}\\
			\end{tabular}

			\caption{Transactions (en millions)}
		\end{table}

		\vspace{-.5cm}

		\begin{table}
			\centering

			\begin{tabular}{l|cccccccc|c}
				\hline
				\diagbox{Mécanisme}{Conteneur} & A & B & C & D & E & F & G & H & Total\\
				\hline
				Aucune limite fixée & 42\% & 23\% & 8\% & 35\% & 69\% & 39\% & 62\% & \textcolor{struct}{71\%} & 44\%\\
				\texttt{Max} limite & \textcolor{struct}{60\%} & 35\% & 5\% & 37\% & 73\% & 15\% & 53\% & 35\% & 39\%\\
				\texttt{Soft} limite & 56\% & 42\% & 11\% & 35\% & 64\% & 16\% & 53\% & 32\% & 39\%\\
				MemOpLight & 58\% & \textcolor{struct}{44\%} & \textcolor{struct}{42\%} & \textcolor{struct}{52\%} & \textcolor{struct}{76\%} & \textcolor{struct}{53\%} & \textcolor{struct}{67\%} & 67\% & \textcolor{struct}{57\%}\\
			\end{tabular}

			\caption{Satisfaction (en pourcent)}
		\end{table}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Étude des paramètres de MemOpLight}

		MemOpLight possède deux paramètres :
		\begin{itemize}
			\item Le pourcentage de mémoire réclamée chaque période.
			\item La durée d'une période de réclamation.
		\end{itemize}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Évaluation}
		\framesubtitle{Pourcentage de mémoire réclamée : résultats}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{pair_mechanism_percent_1_transactions-fig1.pdf}

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{pair_mechanism_percent_1_memory-fig1.pdf}

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			1\%
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{pair_mechanism_percent_10_transactions-fig1.pdf}

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{pair_mechanism_percent_10_memory-fig1.pdf}

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			10\%
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Pourcentage de mémoire réclamée : conclusion}

		\begin{small}
			\begin{table}
				\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
					\hline
					Phases & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
					\hline
					Conteneur & A & B & A & B & A & B & A & B & A & B & B\\
					\hline
					Charge reçue & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
					\hline
					1\% & \textcolor{struct}{1295} & 881 & \textcolor{struct}{1645} & 197 & \textcolor{struct}{1305} & 965 & 196 & 200 & 1364 & 985 & 1481\\
					10\% & 1288 & 897 & 1629 & 197 & 1300 & \textcolor{struct}{973} & 196 & 198 & \textcolor{struct}{1371} & 995 & \textcolor{struct}{1600}\\
					$|1\% - 10\%|$ & 7 & 16 & 16 & 0 & 5 & 8 & 0 & 2 & 7 & 10 & 119\\
				\end{tabular}

				\caption{Débit}
			\end{table}
		\end{small}

		\bigskip

		\onslide<2->{Conclusions :}
		\begin{itemize}
			\item<3-> Le pourcentage de mémoire réclamé n'a pas un grand impact sur les performances.
			\item<4> Ce pourcentage est uniquement indicatif, le noyau fait ce qu'il peut.
		\end{itemize}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Évaluation}
		\framesubtitle{Période de réclamation : résultats}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_transactions-fig3.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_memory-fig3.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			1 seconde
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_period_10_transactions-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_period_10_transactions-fig3.pdf}%

				Débit
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics<1>[width = \textwidth, height = .35\textheight]{pair_mechanism_period_10_memory-fig1.pdf}%
				\includegraphics<2>[width = \textwidth, height = .35\textheight]{pair_mechanism_period_10_memory-fig3.pdf}%

				Mémoire
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			10 secondes
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Période de réclamation : conclusion}

		\begin{small}
			\begin{table}
				\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
					\hline
					Phases & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
					\hline
					Conteneur & A & B & A & B & A & B & A & B & A & B & B\\
					\hline
					Charge reçue & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
					\hline
					\SI{1}{\second} & \textcolor{struct}{1299} & 894 & \textcolor{struct}{1638} & \textcolor{struct}{197} & 1295 & 961 & 196 & 198 & 1362 & 982 & \textcolor{struct}{1476}\\
					\SI{10}{\second} & 1286 & 894 & 1456 & 196 & 1303 & 953 & 196 & \textcolor{struct}{200} & 1367 & 969 & 1115\\
					$|\SI{1}{\second} - \SI{10}{\second}|$ & 13 & 0 & 182 & 1 & 8 & 8 & 0 & 2 & 5 & 13 & 361\\
				\end{tabular}

				\caption{Débit}
			\end{table}
		\end{small}

		\bigskip

		\onslide<2->{Conclusions :}
		\begin{itemize}
			\item<3-> Plus la période est grande moins bonnes sont les performances.
			\item<4> La période doit coller au plus près des variations de charge.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{Étude d'un choix d'implémentation de MemOpLight}

		Design :
		\begin{itemize}
			\item Avec MemOpLight, nous avons choisi de réclamer la mémoire des conteneurs jaunes.
			\item Ainsi, nous espérons diminuer au plus vite le nombre de conteneur rouges.
		\end{itemize}

		\onslide<2->{
			Question :
			\begin{itemize}
				\item Que se passe-t-il si on ne réclame pas les conteneurs jaunes ?
			\end{itemize}
		}

		\only<3>{
			Expérience :
			\begin{itemize}
				\item Nous proposons donc MemOpLightNoYellow, où les conteneurs jaunes ne sont pas réclamés.
			\end{itemize}
		}
	\end{frame}

	\begin{frame}[t]
		\frametitle{Évaluation}
		\framesubtitle{MemOpLightNoYellow : résultats}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions-fig4.pdf}

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{mechanism_color-fig1.pdf}

				Satisfaction
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLight
		\end{center}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{mechanism_stacked_transactions_no_yellow.pdf}

				Transactions
			\end{column}
			\begin{column}{.5\textwidth}
				\centering

				\includegraphics[width = \textwidth, height = .35\textheight]{mechanism_color_no_yellow.pdf}

				Satisfaction
			\end{column}
		\end{columns}

		\vspace{-.5cm}

		\begin{center}
			MemOpLightNoYellow
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{Évaluation}
		\framesubtitle{MemOpLightNoYellow : conclusion}

		\begin{small}
			\begin{table}
				\centering

				\begin{tabular}{l|cccccccc|c}
					\hline
					\diagbox{Mécanisme}{Conteneur} & A & B & C & D & E & F & G & H & Total\\
					\hline
					MemOpLight & \textcolor{struct}{17.5} & \textcolor{struct}{17.3} & \textcolor{struct}{18.4} & \textcolor{struct}{13.7} & \textcolor{struct}{20.2} & \textcolor{struct}{16.8} & \textcolor{struct}{8.6} & \textcolor{struct}{10.1} & \textcolor{struct}{122.6}\\
					MemOpLightNoYellow & 16.7 & 17.0 & 18.1 & 13.4 & 19.0 & 16.3 & 8.4 & 10.0 & 118.9\\
				\end{tabular}

				\caption{Transactions}
			\end{table}

			\vspace{-.5cm}

			\begin{table}
				\centering

				\begin{tabular}{l|cccccccc|c}
					\hline
					\diagbox{Mécanisme}{Conteneur} & A & B & C & D & E & F & G & H & Moyenne\\
					\hline
					MemOpLight & \textcolor{struct}{58} & \textcolor{struct}{44} & \textcolor{struct}{42} & \textcolor{struct}{52} & \textcolor{struct}{76} & \textcolor{struct}{53} & \textcolor{struct}{67} & \textcolor{struct}{67} & \textcolor{struct}{57}\\
					MemOpLightNoYellow & 54 & 38 & 35 & 48 & 75 & 45 & 63 & 59 & 52\\
				\end{tabular}

				\caption{Satisfaction}
			\end{table}
		\end{small}

		\onslide<2->{Conclusions :}
		\begin{itemize}
			\item<3-> Réclamer les conteneurs jaunes permet de récupérer plus de mémoire pour les conteneurs rouges.
			\item<4> Il peut y avoir un effet de seuil avec la mémoire des conteneurs jaunes.
		\end{itemize}
	\end{frame}

	\section{Conclusion}
	\begin{frame}
		\frametitle{Plan}

		\tableofcontents[currentsection]
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}
		\framesubtitle{Travaux réalisés}

		Travaux réalisés :
		\begin{itemize}
			\item Nous avons montré le problème des conteneurs : l'isolation sans la consolidation.
			\item Nous avons proposé l'algorithme de MemOpLight qui est basé sur un retour applicatif.
			\item Nous avons codé des sondes pour mesurer les performances des conteneurs.
			\item Nous avons modifié le mécanisme de réclamation du noyau Linux pour prendre en compte les performances des conteneurs.
			\item MemOpLight améliore les performances de 8.9\% et la satisfaction de 13\%.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}
		\framesubtitle{Perspectives}

		À court terme :
		\begin{itemize}
			\item Lancement d'expériences avec plus d'applications.
			\item Lancement d'expériences plus réalistes.
			\item Ajout d'un historique pour utiliser les séries temporelles.
			\item Ajout d'un cycle d'hystérésis pour résoudre les oscillations.
		\end{itemize}

		À long terme :
		\begin{itemize}
			\item Utiliser MemOpLight pour dimensionner le cache L3.
			\item Utiliser MemOpLight pour partitionner la mémoire entre différents nœuds NUMA.
			\item Prendre en compte d'autres ressources (CPU, bande passante réseau, accès disque, etc.).
		\end{itemize}
	\end{frame}

	\begin{frame}[allowframebreaks, noframenumbering]
		\frametitle{Bibliographie}
		\setbeamertemplate{bibliography item}[text]

		\begin{scriptsize}
			\bibliographystyle{alpha}
			\bibliography{thesis}

			\bigskip
			Certaines images de cette présentation contiennent des images des auteurs suivants : \href{https://www.flaticon.com/authors/smashicons}{Smashicons}, \href{https://www.flaticon.com/authors/good-ware}{Good Ware}, \href{https://icon54.com/}{Icons mind}, \href{https://www.flaticon.com/authors/simpleicon}{SimpleIcon}, \href{https://www.sketchappsources.com/contributor/solarez}{Norman Posselt}, \href{https://www.flaticon.com/fr/auteurs/nikita-golubev}{Nikita Golubev} et \href{http://www.freepik.com}{Freepik} de \href{http://www.flaticon.com}{Flaticon}.
		\end{scriptsize}
	\end{frame}
\end{document}