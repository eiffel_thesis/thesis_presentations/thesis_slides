#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2020 Francis Laniel <francis.laniel@lip6.fr>


my $fd_in;
my $fd_out;

my $filename_in;
my $filename_out;

$filename_in = shift or die "Usage: $0 file.svg";
$filename_out = "${filename_in}.white";

# Open the files.
open $fd_in, '<', $filename_in or die "Cannot open ${filename_in}: $!";
open $fd_out, '>', $filename_out or die "Cannot open ${filename_out}: $!";

# Parse the input file.
while(<$fd_in>){
	my $line;

	$line = $_;

	if($_ =~ /ffffff/){ # If the line contains white content.
		# Make it black.
		$line =~ s/ffffff/000000/g;
	}elsif($_ =~ /000000/){ # If the line contains black content.
		# Make it white.
		$line =~ s/000000/ffffff/g;
	}elsif($_ =~ s/((\s*)id="text\w+")>/$1/){ # If the line contains text.
		# Remove the closing ">" and print the line.
		print ${fd_out} $_;

		# Add a new line to make the text white and close the element.
		$line = "${2}style=\"fill:#ffffff\">\n";
	}

	# Print the line read from $fd_in into $fd_out.
	print ${fd_out} $line;
}

close $fd_in or warn "Problem while closing ${filename_in}: $!";
close $fd_out or warn "Problem while closing ${filename_out}: $!";

rename $filename_out, $filename_in;